#comandos utilizados no projeto:
# gerar modulos:
# C:\desenv\Java\openjdk-11.0.1
# cd F:\01.DESENV\projetos\java\codigo-fonte\workspace_2019\sts4-4.1.2\sicop-api
# java -jar target\sicop-0.0.1-SNAPSHOT.jar --spring.datasource.username=sicop --spring.datasource.password=sicop

# criar modulos
ng g m admin
ng g module shared
ng g module core

# componentes do modulo admin
#
# criar componente usuários
ng g c admin/usuarios
ng g c core/navbar --skipTests=true
ng g c core/home --skipTests=true
ng g c core/footer --skipTests=true
# componente de servicos
ng g c shared/messages --skipTests=true
# servicos messages
ng generate service shared/message --skipTests=true



ng g c dg-primeng --skipTests=true

# criando servicos
ng g service common/messageservice --skipTests=true
# service usuarios
ng g s admin/usuarios --skipTests=true

# criando classe usuario
ng generate class core/models/usuario
ng generate class core/models/Perfil --skipTests=true



