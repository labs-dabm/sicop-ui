import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, map, tap, delay } from 'rxjs/operators';

import { Usuario } from '../core/models/usuario';
import { environment } from '../../environments/environment';
import { MessageService } from '../shared/message.service';


@Injectable({
  providedIn: 'root'
})
export class UsuariosService {

  private readonly URL = `${environment.API}usuarios`;

  constructor(
    private httpClient: HttpClient,
    private messageService: MessageService) { }

  getUsuarios(): Observable<Usuario[]> {
    // TODO: send the message _after_ fetching the users
    this.messageService.add('UsuariosService: Buscando Usuários');

    return this.httpClient.get<Usuario[]>(this.URL)
      .pipe(
        // para testar atraso no carregamento
        delay(2000),
        // debugar
        tap(console.log),
        // pega o erro
        catchError(this.handleError<Usuario[]>('getUsuarios', []))
      );
  }

  /** Log a HeroService message with the MessageService */
  private log(message: string) {
    this.messageService.add(`UsuariosService: ${message}`);
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
