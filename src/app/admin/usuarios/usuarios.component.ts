import { Component, OnInit } from '@angular/core';
import { catchError } from 'rxjs/operators';
import { Observable, empty, of } from 'rxjs';

import { NgbModal, NgbModalRef, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';

import { UsuariosService } from '../usuarios.service';
import { Usuario } from '../../core/models/usuario';
import { Usuarios1Service } from './../usuarios1.service';
import { MessageService } from './../../shared/message.service';
import { AlertModalService } from './../../shared/alert-modal.service';
import { AlertModalComponent } from './../../shared/alert-modal/alert-modal.component';


@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.css'],
  preserveWhitespaces: true
})
export class UsuariosComponent implements OnInit {

  users$: Observable<Usuario[]>;

  ngbModalRef: NgbModalRef;

  constructor(
    private usuariosService: UsuariosService,
    private usuarios1Service: Usuarios1Service,
    private modalService: NgbModal,
    private alertService: AlertModalService,
    private messageService: MessageService,
    public config: NgbModalConfig
  ) {
    // customize default values of modals used by this component tree
    // config.backdrop = 'static';
    // config.keyboard = false;
   }

  ngOnInit() {
    this.consultar();
  }

  consultar() {
    // this.usuariosService.consultar()
    //  .subscribe(resposta => this.users = resposta);

    this.log(`método consultar()`);

    this.users$ = this.usuarios1Service.getUsuarios().pipe(
      catchError(this.handleError<Usuario[]>('consultar', []))
    );
  }

  open(message: string) {
    // const modalRef = this.modalService.open(AlertModalComponent);
    // modalRef.componentInstance.message = message;
     this.alertService.showAlertDanger(message);
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);
      this.open(
        `Ocorreu falha em ${operation},
        Error: ${error.name}`
      );

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** Log a HeroService message with the MessageService */
  private log(message: string) {
    this.messageService.add(`UsuariosComponent: ${message}`);
  }

}
