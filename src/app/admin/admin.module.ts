import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InputTextModule } from 'primeng/inputtext';
import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';

import { UsuariosComponent } from './usuarios/usuarios.component';
import { AdminRoutingModule } from './admin-routing.module';
import { UsuariosService } from './usuarios.service';
import { Usuarios1Service } from './usuarios1.service';

@NgModule({
  declarations: [
    UsuariosComponent
  ],
  imports: [
    // import do angular
    FormsModule,
    CommonModule,
    // import de terceiros
    TableModule,
    ButtonModule,
    InputTextModule,
    // imports do projeto
    // modulo de rotas do modulo admin
    AdminRoutingModule,
  ],
  exports: [
    UsuariosComponent
  ],
  providers: [
    UsuariosService,
    Usuarios1Service
  ]
})
export class AdminModule {
}
