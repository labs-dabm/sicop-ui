import { Injectable } from '@angular/core';

// O serviço expõe seu cache messages dois métodos: um para add()
// uma mensagem para o cache e outro para clear().
@Injectable({
  providedIn: 'root'
})
export class MessageService {

  messages: string[] = [];

  add(message: string) {
    this.messages.push(message);
  }
  clear() {
    this.messages = [];
  }
}
