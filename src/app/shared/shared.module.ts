import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgbModalModule, NgbAlertModule } from '@ng-bootstrap/ng-bootstrap';

import { MessagesComponent } from './messages/messages.component';
import { AlertModalComponent } from './alert-modal/alert-modal.component';
import { AlertModalService } from './alert-modal.service';
import { MessageService } from './message.service';

@NgModule({
  declarations: [MessagesComponent, AlertModalComponent],
  imports: [
    CommonModule,

    NgbModalModule,
    NgbAlertModule,

  ],
  exports: [
    MessagesComponent,
    AlertModalComponent
  ],
  providers: [
    MessageService,
    AlertModalService
  ],
  entryComponents: [
    AlertModalComponent
  ]
})
export class SharedModule { }
