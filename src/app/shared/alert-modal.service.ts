import { AlertModalComponent } from './alert-modal/alert-modal.component';
import { Injectable } from '@angular/core';

import { NgbModal, NgbModalRef, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';

export enum AlertTypes {
  DANGER = 'danger',
  SUCCESS = 'success'
}

@Injectable({
  providedIn: 'root'
})
export class AlertModalService {

  constructor(
    private modalService: NgbModal,
    public config: NgbModalConfig
    ) {
      // customize default values of modals used by this component tree
      config.backdrop = 'static';
      config.keyboard = false;
     }

  private showAlert(message: string, type: string) {

    const ngbModalRef: NgbModalRef = this.modalService.open(AlertModalComponent);

    ngbModalRef.componentInstance.message = message;
    ngbModalRef.componentInstance.type = type;
  }

  showAlertDanger(message: string) {
    this.showAlert(message, AlertTypes.DANGER);
  }

  showAlertSuccess(message: string) {
    this.showAlert(message, AlertTypes.SUCCESS);
  }
}
