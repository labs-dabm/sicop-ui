import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  navbarOpen = false;

  isNavbarCollapsed = true;

  constructor() { }

  ngOnInit() {
  }

  /**
   * https://medium.com/@tiboprea/build-a-responsive-bootstrap-4-navbar-in-angular-5-without-jquery-c59ad35b007
   * https://stackoverflow.com/questions/45951341/cant-toggle-navbar-in-bootstrap-4-in-angular
   */
  toggleNavbar() {
    this.navbarOpen = !this.navbarOpen;
  }
}
