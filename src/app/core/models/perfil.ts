export class Permissao {
  id: number;
  nome: string;
  listaPerfis: Perfil[];
}

export class Perfil {
  id: number;
  nome: string;
  descricao: string;
  usuarios: string;
  listaPermissoes: Permissao[];
}
