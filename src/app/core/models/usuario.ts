import { Perfil, Permissao } from './perfil';

export class CorpoQuadro {
  id: number;
  nome: string;
  sigla: string;
  ordem: number;
}

export class PostoGraduacao {
  id: number;
  nome: string;
  sigla: string;
  ordem: number;
}

export class OrganizacaoMilitar {
  id: number;
  nome: string;
  sigla: string;
  codigo: string;
  indicativoNaval: string;
  ativo = true;
}

export class Especialidade {
  id: number;
  nome: string;
  sigla: string;
  corpoQuadro = new CorpoQuadro();
}

export class Usuario {
  id: number;
  login: string;
  senha: string;
  confirmaSenha: string;
  nomeCompleto: string;
  nomeGuerra: string;
  nip: string;
  corpoQuadro = new CorpoQuadro();
  postoGraduacao = new PostoGraduacao();
  organizacaoMilitar = new OrganizacaoMilitar();
  especialidade = new Especialidade();
  email: string;
  dataCadastro: Date;
  // listaPerfis: Perfil[];
}
