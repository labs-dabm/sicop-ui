# Sistema de Gerenciamento de Contratos e Processos - SICOP 

## Propósito:

O propósito do SISCOP-UI é aplicar o conhecimento adquirido das novas tecnologias utilizadas na Modernização do SINGRA.

## Tecnologias Utilizadas no Projeto

Para produção do sistema de **SICOP**, estão sendo utilizadas ferramentas reconhecidas no mercado:

* **Node.js ver.: 11.14.0) e npm ver.: 6.7.0** - https://nodejs.org/en
* **Angular CLI ver.: 7.3.8** - <https://angular.io/guide/architecture>
* **PrimeNG ver.: 7.1.2** - <https://www.primefaces.org/primeng/#/setup>
* **Bootstrap ver.: 4.3.1** - <https://getbootstrap.com/docs/4.3/getting-started/introduction/
* **EasyUI ver.: 1.2.0** - <https://www.jeasyui.com/download/ng.php>

## Arquitetura utilizada no Projeto

![](wiki/assets/modules.png)

Abaixo a estrutura de diretórios proposta para o projeto com base na figura acima.

```
|-- modules

    |-- module1
        |-- [+] components
        |-- module1.service.ts
        |-- module1.module.ts
        |-- module1.routes.ts

    |-- module2 
        |-- [+] components
        |-- module2.service.ts
        |-- module2.module.ts
        |-- module2.routes.ts

|-- shared
        |-- [+] components
        |-- [+] mocks
        |-- [+] models
        |-- [+] directives
        |-- [+] pipes

|-- core
        |-- [+] authentication
        |-- [+] footer
        |-- [+] guards
        |-- [+] http
        |-- [+] interceptors
        |-- [+] mocks
        |-- [+] services
        |-- [+] header

|-- app.module.ts
|-- app.component.ts
```

### Módulos - Lazy Load

Módulos lazy loaded ajudam a diminuir o tempo de inicialização da aplicação. Com o lazy load, o aplicativo não precisa carregar tudo de uma só vez. Ele só vai carregar o que o usuário espera ver. O módulo só irá carregar quando o usuário navegar para sua rota. E essa estrutura é coesa, mantém os grupos de interesse dentro de um mesmo local.

```
|-- ata // Module
    |-- inserir
        |-- inserir-ata.component .|html|scss|spec|.ts
    |-- editar
        |-- editar-ata.component .|html|scss|spec|.ts
    |-- detalhes
        |-- detalhes-ata.component .|html|scss|spec|.ts
    |-- ata.service.ts
    |-- ata.module.ts
    |-- ata.routes.ts
```

### Core

O **core module** deve conter serviços **singleton**, componentes universais e outros recursos em que há uma instância única. Autenticação, header, interceptors são exemplos de componentes que terá apenas uma instância ativa para a aplicação e será utilizado praticamente por todos os modules.

### Shared

O **shared module** é onde todos os componentes compartilhados, *pipes, filters e services* devem ir. O *shared* pode ser importado em qualquer module. Assim esses itens serão reutilizados. O *shared module* deve ser independente do restante do aplicativo. Portanto, não deve ter referências de outro módulo.

## Instalando o Projeto

Para executar e testar o projeto: 

```bash
# clonando o projeto
git clone git@gitlab.com:labs-dabm/sicop-ui.git
cd sicop-ui

# instalando as dependências do projeto
npm install

# iniciar o projeto no navegador
ng serve -o
# inicar em uma porta especifica
ng serve --port 8181
```

### Iniciando o Git Flow no projeto

Este projeto está trabalhando com o Git Flow, portanto para que tenha um melhor aproveitamento favor executar os procedimentos abaixo.

```bash
# verficar os branches trazidos do repositório com os commits
$ git branch -a –v
	
# criando e alternando para o branch develop – usado como referência de desenv.
$ git checkout -b develop origin/develop
	
# Iniciando de fato o git flow no projeto clonado. Nessa etapa deixa com as opções default
$ git flow init
```

